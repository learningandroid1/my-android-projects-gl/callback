package com.example.callback

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView: TextView = findViewById(R.id.textView)

        fun someFunction(){
            val word = "nice"
            val letter = "n"
            if (word.contains(letter)){
                Toast.makeText(this@MainActivity, "Success", Toast.LENGTH_LONG).show()}
            else {Toast.makeText(this@MainActivity, "Failure", Toast.LENGTH_LONG).show()}
        }

        textView.setOnClickListener{
            someFunction()
        }
    }
}